/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.edu.ifrn.app;

import br.edu.ifrn.app.classes.Calculo;
import br.edu.ifrn.app.views.HomeJFrame;
import java.util.HashMap;
import java.util.Map;
import java.util.TreeMap;

/**
 *
 * @author claudio
 */
public class App {

    /**
     * @param args the command line arguments
     */
    public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(HomeJFrame.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(HomeJFrame.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(HomeJFrame.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(HomeJFrame.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>
        //</editor-fold>

        /* Create and display the form */
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {

                Map<Double, Double> dadosEstatisticos = new HashMap<>();

                dadosEstatisticos.put(1.2, 96.83);
                dadosEstatisticos.put(2.1, 98.02);
                dadosEstatisticos.put(3.5, 89.30);
                dadosEstatisticos.put(5.3, 80.90);
                dadosEstatisticos.put(7.6, 72.74);
                dadosEstatisticos.put(11.0, 68.68);
                dadosEstatisticos.put(16.0, 61.48);
                dadosEstatisticos.put(24.1, 56.21);
                dadosEstatisticos.put(33.8, 51.00);
                dadosEstatisticos.put(44.00, 47.70);
                dadosEstatisticos.put(55.3, 42.78);
                dadosEstatisticos.put(69.2, 39.43);
                dadosEstatisticos.put(86.0, 41.24);

                Map<Double, Double> dadosEstatisticosOrdenados = new TreeMap<>(dadosEstatisticos);

                Calculo calculo = new Calculo();

                new HomeJFrame(dadosEstatisticosOrdenados, calculo).setVisible(true);
            }
        });
    }

}

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.edu.ifrn.app.classes;

import java.util.HashMap;
import java.util.Map;
/**
 *
 * @author claudio
 */
public class Calculo {
    
    public Double result = 0.0;

    public Calculo() {

    }

    public Double somatorioX(Map<Double, Double> dadosEstatisticos) {  
        this.result = 0.0;
        for (Double key : dadosEstatisticos.keySet()) {
           this.result += key;
        }
        return this.result;
    }

    public Double somatorioXquadrado(Map<Double, Double> dadosEstatisticos) {
        this.result = 0.0;
        for (Double key : dadosEstatisticos.keySet()) {
            this.result += Math.pow(key, 2);
        }
        return this.result;
    }

    public Double somatorioY(Map<Double, Double> dadosEstatisticos) {
        this.result = 0.0;
        for (Double key : dadosEstatisticos.keySet()) {
            this.result += dadosEstatisticos.get(key);
        }
        return this.result;
    }

    public Double somatorioYquadrado(Map<Double, Double> dadosEstatisticos) {
        this.result = 0.0;
        for (Double key : dadosEstatisticos.keySet()) {
            this.result += Math.pow(dadosEstatisticos.get(key), 2);
        }
        return this.result;
    }

    public Double multXY(Map<Double, Double> dadosEstatisticos) {
        this.result = 0.0;
        for (Double key : dadosEstatisticos.keySet()) {
            this.result += (dadosEstatisticos.get(key) * key);
        }
        return this.result;
    }

    public Double mediaDadosMap(Map<Double, Double> dadosEstatisticos, boolean x, boolean y) {
        Double media = 0.0;
        // Se Y for diferente de NULL, a média será fetia em cima dos dados de Y no  map
        if (y) {
            for (Double key : dadosEstatisticos.keySet()) {
                media += dadosEstatisticos.get(key);
            }
        }
        // Se x for diferente de NULL, a média será fetia em cima dos dados de X no  map
        if (x) {
            for (Double key : dadosEstatisticos.keySet()) {
                media += key;
            }
        }
        media /= dadosEstatisticos.size();
        return media;
    }

    public Double desvioPadrao(Map<Double, Double> dadosEstatisticos) {
        Double variancia = variancia(dadosEstatisticos);
        return Math.sqrt(variancia);
    }

    public Double variancia(Map<Double, Double> dadosEstatisticos) {
        Double media = mediaDadosMap(dadosEstatisticos, false, true);
        Double variancia = 0.0;
        for (Double key : dadosEstatisticos.keySet()) {
            variancia += Math.pow(dadosEstatisticos.get(key) - media, 2);
        }
        this.result = variancia / dadosEstatisticos.size();
        return this.result;
    }

    public Double coeficienteCorrelacao(Map<Double, Double> dadosEstatisticos) {
        Double r = 0.0;
        Double x = somatorioX(dadosEstatisticos);
        Double y = somatorioY(dadosEstatisticos);
        Double x2 = somatorioXquadrado(dadosEstatisticos);
        Double y2 = somatorioYquadrado(dadosEstatisticos);
        Double m = multXY(dadosEstatisticos);
        int size = dadosEstatisticos.size();

        r = ((size * m)- (x * y));
        r /= (Math.sqrt((size * x2) - Math.pow(x, 2)) * (Math.sqrt(((size * y2) - (Math.pow(y, 2))))));

        return r;
    }

    public Map<Double, Double> retaRegresao(Map<Double, Double> dadosEstatisticos) {
        
        Double m, b, yEstimado;
        Double x = somatorioX(dadosEstatisticos);
        Double y = somatorioY(dadosEstatisticos);
        Double x2 = somatorioXquadrado(dadosEstatisticos);
        Double y2 = somatorioYquadrado(dadosEstatisticos);
        Double multiXY = multXY(dadosEstatisticos);
        Double mediaX = mediaDadosMap(dadosEstatisticos, true, false);
        Double mediay = mediaDadosMap(dadosEstatisticos, false, true);
        int size = dadosEstatisticos.size();

        m = ((size * multiXY) - (x * y)) / (size * x2 - Math.pow(x, 2));
        b = mediay - (m * mediaX);

        Map<Double, Double> dadosEstimados = new HashMap<>();
       
        for (Double key : dadosEstatisticos.keySet()) {
            dadosEstimados.put(key, (m * key + b));
        }
        return dadosEstimados;
    }

    public Double erroEstimativa(Map<Double, Double> dadosEstatisticos) {
        Double somatorio = somatorioEstimado(dadosEstatisticos, retaRegresao(dadosEstatisticos));
        int size = dadosEstatisticos.size();

        Double erroEstimativa = Math.sqrt(somatorio / (size - 2));

        return erroEstimativa;
    }

    public Double somatorioEstimado(Map<Double, Double> dadosEstatisticos, Map<Double, Double> dadosEstimados) {
        Double soma = 0.0;
        for (Double key : dadosEstatisticos.keySet()) {
            soma += (Math.pow(dadosEstatisticos.get(key) - dadosEstimados.get(key), 2));
        }
        return soma;
    }
}

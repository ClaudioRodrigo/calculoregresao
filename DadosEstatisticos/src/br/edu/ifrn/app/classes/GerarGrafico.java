/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.edu.ifrn.app.classes;

import java.awt.Color;
import java.util.HashMap;
import java.util.Map;
import org.jfree.chart.ChartFactory;
import org.jfree.chart.ChartPanel;
import org.jfree.chart.JFreeChart;
import org.jfree.chart.block.BlockBorder;
import org.jfree.chart.plot.CategoryPlot;
import org.jfree.chart.plot.PlotOrientation;
import org.jfree.data.category.DefaultCategoryDataset;
import org.jfree.ui.RectangleInsets;

/**
 *
 * @author claudio
 */
public class GerarGrafico {

    // cria o conjunto de dados
    public DefaultCategoryDataset ds = new DefaultCategoryDataset();
    // cria o gráfico
    public JFreeChart grafico;

    public Map<Double, Double> dadosEstatisticos = new HashMap<>();

    public GerarGrafico(Map<Double, Double> dadosEstatisticos) {
        super();
        this.dadosEstatisticos = dadosEstatisticos;
    }

    public ChartPanel grafico(String title, String legendaX, String legendaY) {

        this.grafico = ChartFactory.createLineChart(title, legendaX,
                legendaY, ds, PlotOrientation.VERTICAL, true, true, false);

        Calculo c = new Calculo();
        Map<Double, Double> dadosEstimados = c.retaRegresao(this.dadosEstatisticos);

        for (Double key : this.dadosEstatisticos.keySet()) {
            ds.addValue(this.dadosEstatisticos.get(key), "Dados reais", key);
            ds.addValue(dadosEstimados.get(key), "Dados estimados", key);
            System.out.println(key);
        }

        ChartPanel painel = new ChartPanel(graficoPersonalizado(this.grafico));

        return painel;
    }

    public JFreeChart graficoPersonalizado(JFreeChart grafico) {
        CategoryPlot plot = grafico.getCategoryPlot();
        plot.setDomainGridlinePaint(Color.white);
        plot.setRangeGridlinePaint(Color.white);
        plot.setOutlineVisible(false);
        plot.setBackgroundPaint(Color.WHITE);

        grafico.getLegend().setFrame(BlockBorder.NONE);
        grafico.getLegend().setItemLabelPadding(new RectangleInsets(5.0, 2.0, 10.0, 9.9));
        grafico.getLegend().setPadding(new RectangleInsets(20.0, 20.0, 0.0, 10.0));

        return grafico;
    }
}
